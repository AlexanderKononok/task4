//
//  ViewController.swift
//  AtmMap
//
//  Created by Alexander Kononok on 9/30/21.
//

import UIKit
import Dispatch

class ViewController: UIViewController {

    let mainView = UIView()
    let map = MapView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        
        setupMainView()
        
        map.setupMapView(mainView: mainView)
        map.checkLocationServices()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.global(qos: .background).async {
            self.map.getAtmCoords()
        }
        DispatchQueue.global(qos: .background).async {
            self.map.getInfoboxCoords()
        }
        DispatchQueue.global(qos: .background).async {
            self.map.getFilialCoords()
        }
    }
    
    func setupMainView() {
        view.addSubview(mainView)
        mainView.translatesAutoresizingMaskIntoConstraints = false
        mainView.topAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        mainView.leadingAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        mainView.trailingAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        mainView.bottomAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
}
