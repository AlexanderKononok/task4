//
//  MapView.swift
//  AtmMap
//
//  Created by Alexander Kononok on 10/4/21.
//

import UIKit
import MapKit
import CoreLocation

class MapView: UIView {
    
    private let mapView = MKMapView()
    private let locationManager = CLLocationManager()
    private let regionInMeters: Double = 3000
    
    private var coordsATM = [[String]]()
    private var coordsInfobox = [[String]]()
    private var coordsFilial = [[String]]()
    
    func setupMapView(mainView: UIView) {
        mainView.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.topAnchor.constraint(equalTo: mainView.topAnchor).isActive = true
        mapView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor).isActive = true
        mapView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor).isActive = true
        
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        mapView.delegate = self
    }
    
    private func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            // Show alert letting the user know they have to turn this on
        }
    }
    
    private func checkLocationAuthorization() {
        switch locationManager.authorizationStatus {
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
            centerViewOnUserLocation()
            locationManager.startUpdatingLocation()
            break
        case .denied:
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted:
            break
        case .authorizedAlways:
            break
        @unknown default:
            print("Error: unknown value")
        }
    }
    
    private func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion(center: location,
                                            latitudinalMeters: regionInMeters,
                                            longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
    }
    
    private func addATM() {
        if coordsATM.count > 0 {
            for coords in coordsATM {
                let atmAnnotation = MKPointAnnotation()
                atmAnnotation.title = "Belarusbank atm"
                
                if let latitude = Double(coords[0]), let longitude = Double(coords[1]) {
                    atmAnnotation.coordinate = CLLocationCoordinate2D(
                        latitude: latitude,
                        longitude: longitude)
                }
                
                mapView.addAnnotation(atmAnnotation)
            }
        }
    }
    
    private func addInfobox() {
        if coordsInfobox.count > 0 {
            for coords in coordsInfobox {
                let infoboxAnnotation = MKPointAnnotation()
                infoboxAnnotation.title = "Belarusbank infobox"
                
                if let latitude = Double(coords[0]), let longitude = Double(coords[1]) {
                    infoboxAnnotation.coordinate = CLLocationCoordinate2D(
                        latitude: latitude,
                        longitude: longitude)
                }
                
                mapView.addAnnotation(infoboxAnnotation)
            }
        }
    }
    
    private func addFilials() {
        if coordsFilial.count > 0 {
            for coords in coordsFilial {
                let filialAnnotation = MKPointAnnotation()
                filialAnnotation.title = "Belarusbank filial"
                
                if let latitude = Double(coords[0]), let longitude = Double(coords[1]) {
                    filialAnnotation.coordinate = CLLocationCoordinate2D(
                        latitude: latitude,
                        longitude: longitude)
                }
                
                mapView.addAnnotation(filialAnnotation)
            }
        }
    }
    
    func getAtmCoords() {
        let session = URLSession.shared
        let cityGomel = "%D0%93%D0%BE%D0%BC%D0%B5%D0%BB%D1%8C"
        let url = "https://belarusbank.by/api/atm?city=\(cityGomel)"
        if let url = URL(string: url) {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            let task = session.dataTask(with: url) { data, response, error in
                do {
                    let data = try Data(contentsOf: url)
                    
                    if let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) {
                        if let atmCoords = jsonData as? [[String: String]] {
                            for coords in atmCoords {
                                self.coordsATM.append([coords["gps_x"]!, coords["gps_y"]!])
                            }
                        }
                        
                        self.addATM()
                    }
                } catch {
                    print("JSON error: \(error.localizedDescription)")
                }
            }
            
            task.resume()
        }
    }
    
    func getInfoboxCoords() {
        let session = URLSession.shared
        let cityGomel = "%D0%93%D0%BE%D0%BC%D0%B5%D0%BB%D1%8C"
        let url = "https://belarusbank.by/api/infobox?city=\(cityGomel)"
        if let url = URL(string: url) {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            let task = session.dataTask(with: url) { data, response, error in
                do {
                    let data = try Data(contentsOf: url)
                    
                    if let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) {
                        print("1")
                        if let infoboxCoords = jsonData as? [[String: String]] {
                            print("2")
                            print(infoboxCoords)
                            for coords in infoboxCoords {
                                self.coordsInfobox.append([coords["gps_x"]!, coords["gps_y"]!])
                            }
                        }
                        print(self.coordsInfobox)
                        self.addInfobox()
                    }
                } catch {
                    print("JSON error: \(error.localizedDescription)")
                }
            }
            
            task.resume()
        }
    }
    
    func getFilialCoords() {
        let session = URLSession.shared
        let cityGomel = "%D0%93%D0%BE%D0%BC%D0%B5%D0%BB%D1%8C"
        let url = "https://belarusbank.by/api/filials_info?city=\(cityGomel)"
        if let url = URL(string: url) {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            let task = session.dataTask(with: url) { data, response, error in
                do {
                    let data = try Data(contentsOf: url)
                    
                    if let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) {
                        if let filialCoords = jsonData as? [[String: String]] {
                            for coords in filialCoords {
                                self.coordsFilial.append([coords["GPS_X"]!, coords["GPS_Y"]!])
                            }
                        }
                        
                        self.addFilials()
                    }
                } catch {
                    print("JSON error: \(error.localizedDescription)")
                }
            }
            
            task.resume()
        }
    }
    
}

extension MapView: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let center = CLLocationCoordinate2D(
            latitude: location.coordinate.latitude,
            longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion.init(center: center,
                                             latitudinalMeters: regionInMeters,
                                             longitudinalMeters: regionInMeters)
        mapView.setRegion(region, animated: true)
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorization()
    }
}

extension MapView: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "AnnotationView")
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView")
        }
        
        if let title = annotation.title, title == "Belarusbank atm" {
            annotationView?.image = UIImage(named: "local_atm")
        } else if let title = annotation.title, title == "Belarusbank infobox" {
            annotationView?.image = UIImage(named: "infobox")
        } else if let title = annotation.title, title == "Belarusbank filial" {
            annotationView?.image = UIImage(named: "filial")
        } else if annotation === mapView.userLocation {
            annotationView?.image = UIImage(named: "my_location")
        }
        
        annotationView?.canShowCallout = true
        
        return annotationView
    }
}
